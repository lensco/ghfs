#!/bin/bash

if [[ "$CORDOVA_PLATFORMS" == "android" ]]
then

    # AndroidManifest.xml
    ANDROIDMANIFEST='platforms/android/AndroidManifest.xml'
    ENABLED=`cat $ANDROIDMANIFEST | grep 'android:largeScreens="true"'`
    if [[ "$ENABLED" != "" ]]
    then

        echo "Fixing Android "AndroidManifest.xml" to limit screen sizes"
        sed -i '' -e 's/android:largeScreens="true"/android:largeScreens="false"/' $ANDROIDMANIFEST
        sed -i '' -e 's/android:xlargeScreens="true"/android:xlargeScreens="false"/' $ANDROIDMANIFEST

    fi


fi