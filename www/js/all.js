/**
 * Ionic GHFS App
 */

angular.module('ghfs.controllers', []);
angular.module('ghfs.services', []);
angular.module('ghfs.filters', []);
angular.module('ghfs.providers', []);

angular.module('ghfs', ['ionic','ionic.service.core', 'ghfs.controllers', 'ghfs.services', 'ghfs.filters', 'ghfs.providers', 'ngCordova'])

.config(function($ionicConfigProvider) {
	// Native scrolling
	if( ionic.Platform.isAndroid() ) {
		$ionicConfigProvider.scrolling.jsScrolling(false);
	}
});



(function() {

	'use strict';

	angular.module('ghfs').config(function($stateProvider, $urlRouterProvider, $ionicConfigProvider) {

		$stateProvider

		.state('app', {
			url: "/app",
			abstract: true,
			templateUrl: "templates/menu.html",
			controller: 'AppCtrl'
		})

		.state('app.posts', {
			url: "/posts",
			views: {
				'menuContent': {
					templateUrl: "templates/posts.html",
					controller: 'PostsCtrl'
				}
			}
		})

		.state('app.post', {
			url: "/posts/:postId",
			views: {
				'menuContent': {
					templateUrl: "templates/post.html",
					controller: 'PostCtrl'
				}
			},
			changeColor: 'transparent'
		})

		.state('app.categories', {
			url: "/categories",
			views: {
				'menuContent': {
					templateUrl: "templates/categories.html"
				}
			}
		})

		.state('app.page', {
			url: "/page/:slug",
			views: {
				'menuContent': {
					templateUrl: "templates/page.html",
					controller: "PageCtrl"
				}
			}
		})

		.state('app.programs', {
			url: "/programs",
			views: {
				'menuContent': {
					templateUrl: "templates/programs.html"
				}
			}
		})

		.state('app.settings', {
			url: "/settings",
			views: {
				'menuContent': {
					templateUrl: "templates/settings.html"
				}
			}
		})

		.state('app.test', {
			url: "/test",
			views: {
				'menuContent': {
					templateUrl: "templates/test.html",
					controller: "TestCtrl"
				}
			}
		});

		// if none of the above states are matched, use this as the fallback
		$urlRouterProvider.otherwise('/app/posts');
	});
})();
(function() {

	'use strict';

	angular.module('ghfs').run(function($ionicPlatform, $state, $rootScope, $ionicPopup, $ionicConfig, $log, $http, PushNotificationService, localStorageProvider) {

		$ionicPlatform.ready(function() {
			// Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
			// for form inputs)
			// if (window.cordova && window.cordova.plugins.Keyboard) {
			// 	cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
			// }

			ionic.Platform.fullScreen(true, true);

			if (window.StatusBar) {
				StatusBar.styleLightContent();

				// We let the statusbar overlay the webview, yet the class `in-webview` will move everything down
				// That way we have a transparent statusbar that does not overlap the content
				StatusBar.overlaysWebView(true);

			}

			$rootScope.$on('$stateChangeSuccess', function (evt, toState) {
				$log.log('stateChangeSuccess');

				// check for header color on state change
				if (toState.changeColor) {
					$rootScope.secColor = toState.changeColor;
				} else {
					$rootScope.secColor = false;
				}
			});


			$rootScope.showNetworkError = function() {
				$log.error('Showing network error popup');

				$ionicPopup.alert({
					title: "Network issue",
					template: "It appears your internet connection is down, or there is a problem with the Green Heritage Fund Suriname website. Please try again later…",
					okType: "button-clear button-balanced"
				});
			};


			$ionicConfig.backButton.icon('icon-back');


			// Request push notifications
			if (localStorageProvider.get('deviceToken')) {
				$log.log('PushNotificationService.init: already executed');
			} else {
				PushNotificationService.init({
					"android": {
						"senderID": 952573768849,
						"icon": "notification",
						"iconColor": "#58CF90"
					},
					"ios": {
						"alert": "true",
						"badge": "true",
						"sound": "true",
						"clearBadge": "true"
					}
				}).then(function(deviceToken) {
					$log.log('PushNotificationService.init: resolved: deviceToken=' + deviceToken + '');
					localStorageProvider.set('deviceToken', deviceToken);
				}, function(error) {
					$log.warn('PushNotificationService.init: rejected:', JSON.stringify(error));
				});
			}


			// When push notification is received
			$rootScope.onPushnotificationReceived = function(data) {
				$log.log('PushNotificationService.onPushnotificationReceived', data);

				if(!data.additionalData.foreground) {
					$state.go('app.post', {postId: data.additionalData.id});
				}
			};
		});
	});
})();
(function() {

	'use strict';

	angular.module('ghfs')

	.controller('AppCtrl', function($scope, $ionicModal, $timeout, $sce, DataLoader, $rootScope ) {

		// WORDPRESS API URL
		$rootScope.url = 'http://www.greenfundsuriname.org/wp-json/wp/v2/';

		$rootScope.callback = '';

	});

})();
(function() {

	'use strict';

	angular.module('ghfs')

	.controller('PageCtrl', function($scope, $stateParams, DataLoader, $ionicLoading, $rootScope, $sce, localStorageProvider, $log, $timeout, $ionicConfig, $filter ) {

		$scope.slug = $stateParams.slug;

		var pageApi = $rootScope.url + 'pages/?filter[name]=' + $scope.slug + '&' + $rootScope.callback;

		// Fetch remote page
		$scope.loadPage = function() {
			$ionicLoading.show({
				noBackdrop: true
			});

			$log.log("Fetching '" + pageApi + "'");

			DataLoader.get( pageApi ).then(function(response) {
				var response_data = response.data[0];

				$scope.page = response_data;

				// Don't strip post html
				$scope.content = $sce.trustAsHtml(response_data.content.rendered);

				// Add page to our cache with 1 hour expiration time
				localStorageProvider.set(response_data.slug, { date: new Date().getTime() + (60 * 60 * 1000), data: response_data });

				$ionicLoading.hide();

			}, function(response) {
				$ionicLoading.hide();
				$rootScope.showNetworkError();
			});
		}

		// Check the cache and smell if it's fresh
		var cachedPage = localStorageProvider.get( $scope.slug );

		if( cachedPage && (cachedPage.date > new Date().getTime()) ) {
			// Use cached item
			$log.log("Page '" + $scope.slug + "' is in cache!");
			$scope.page = cachedPage.data;
			$scope.content = $sce.trustAsHtml( $scope.page.content.rendered );

		} else {
			// Go get it again
			$log.log("Page '" + $scope.slug + "' is not in cache, let's go get it!");
			$scope.loadPage();
		}


		$scope.$on('$ionicView.beforeEnter', function() {
			// Just empty back button text until we have a better solution
			$ionicConfig.backButton.text("");
		});


		// Pull to refresh
		$scope.doRefresh = function() {

			$timeout( function() {
				$log.log("Refreshing…");
				$scope.loadPage();

				//Stop the ion-refresher from spinning
				$scope.$broadcast('scroll.refreshComplete');

			}, 1000);

		};

	});
})();
(function() {

	'use strict';

	angular.module('ghfs')

	.controller('PostCtrl', function($scope, $stateParams, DataLoader, $ionicLoading, $rootScope, $sce, localStorageProvider, $log, $timeout, $ionicScrollDelegate, $ionicConfig ) {

		$scope.itemID = $stateParams.postId;

		var singlePostApi = $rootScope.url + 'posts/' + $scope.itemID + '?_embed&' + $rootScope.callback;

		// Fetch remote post
		$scope.loadPost = function() {
			$ionicLoading.show({
				noBackdrop: true
			});

			$log.log("Fetching " + singlePostApi);

			DataLoader.get( singlePostApi ).then(function(response) {
				$scope.post = response.data;

				// Don't strip post html
				$scope.content = $sce.trustAsHtml(response.data.content.rendered);

				// Add post to our cache with 1 hour expiration time
				localStorageProvider.set($scope.itemID, { date: new Date().getTime() + (60 * 60 * 1000), data: response.data });

				$ionicLoading.hide();

			}, function(response) {
				$ionicLoading.hide();
				$rootScope.showNetworkError();
			});

		}


		// Check the cache and smell if it's fresh
		var cachedPost = localStorageProvider.get( $scope.itemID );

		if( cachedPost && (cachedPost.date > new Date().getTime()) ) {
			// Use cached item
			$log.log("Post " + $scope.itemID + " is in cache!");
			$scope.post = cachedPost.data;
			$scope.content = $sce.trustAsHtml( $scope.post.content.rendered );

		} else {
			// Go get it again
			$log.log("Post " + $scope.itemID + " is not in cache, let's go get it!");
			$scope.loadPost();
		}


		// Pull to refresh
		$scope.doRefresh = function() {

			$timeout( function() {
				$log.log("Refreshing…");
				$scope.loadPost();

				//Stop the ion-refresher from spinning
				$scope.$broadcast('scroll.refreshComplete');

			}, 1000);

		};


		// Share post
		$scope.sharePost = function(share) {
			$log.log("Sharing post: " + $scope.post.title.rendered, $scope.post.featured_image_thumbnail_url, $scope.post.link)
			if (window.cordova) {
				window.plugins.socialsharing.share(null, $scope.post.title.rendered, $scope.post.featured_image_thumbnail_url, $scope.post.link);
			}
		}


		// Transparent header
		$scope.$on('$ionicView.beforeEnter', function() {
			$('body').addClass('transparent-header');

			// set back button text
			$ionicConfig.backButton.text("Updates");
		});

		$scope.$on('$ionicView.beforeLeave', function() {
			$('body').removeClass('transparent-header');
		});

		$scope.$on('$ionicView.afterEnter', function() {
			var $graphic = $('#featured_image_graphic'),
				graphic_height = $graphic.height(),
				offset = 0,
				alpha = 0,
				$header_backdrop = $('#header-backdrop div');

			$scope.gotScrolled = function() {
				offset = $ionicScrollDelegate.getScrollPosition().top;
				alpha = offset - graphic_height + 164;

				if (alpha > 0 && alpha < 101) {
					$header_backdrop.css('opacity', function() {
						return alpha/100;
					});
				} else if (alpha <= 0) {
					$header_backdrop.css('opacity', 0);
				} else {
					$header_backdrop.css('opacity', 1);
				}
			}
		});

	});
})();
(function() {

	'use strict';

	angular.module('ghfs')

	.controller('PostsCtrl', function( $scope, $http, DataLoader, $timeout, $ionicSlideBoxDelegate, $rootScope, $log, $ionicLoading ) {

		$scope.moreItems = false;
		var paged = 2;

		var postsApi = $rootScope.url + 'posts/?_embed&' + $rootScope.callback;

		// Get all of our posts
		$scope.loadPosts = function() {
			$ionicLoading.show({
				noBackdrop: true
			});

			$log.log("Fetching " + postsApi);

			DataLoader.get( postsApi ).then(function(response) {
				$scope.posts = response.data;

				$scope.doWeHaveUpdates();

				$scope.moreItems = true;

				$ionicLoading.hide();

			}, function(response) {
				$ionicLoading.hide();
				$rootScope.showNetworkError();
			});

		}

		// Let's get some posts
		$scope.loadPosts();


		// Load more (infinite scroll)
		$scope.loadMore = function() {

			if( !$scope.moreItems ) {
				return;
			}

			var pg = paged++;

			$log.log('loadMore ' + pg );

			$timeout(function() {

				DataLoader.get( postsApi + '&page=' + pg ).then(function(response) {

					angular.forEach( response.data, function( value, key ) {
						$scope.posts.push(value);
					});

					if( response.data.length <= 0 ) {
						$scope.moreItems = false;
					}
				}, function(response) {
					$scope.moreItems = false;
					$log.error(response);
				});

				$scope.$broadcast('scroll.infiniteScrollComplete');
				$scope.$broadcast('scroll.resize');

			}, 1000);

		}

		$scope.moreDataExists = function() {
			return $scope.moreItems;
		}

		// Pull to refresh
		$scope.doRefresh = function() {

			$timeout( function() {
				$log.log("Refreshing…");
				$scope.loadPosts();

				//Stop the ion-refresher from spinning
				$scope.$broadcast('scroll.refreshComplete');

			}, 1000);

		};


		// Check if we actually have updates, otherwise show error
		$scope.doWeHaveUpdates = function() {
			if ($scope.posts && $scope.posts.length == 0) {
				$log.error('Posts query returns empty array');

				$scope.weHaveUpdates = false;
			} else {
				$scope.weHaveUpdates = true;
			}
		}

		$scope.$on('$ionicView.enter', function() {
			$scope.doWeHaveUpdates();
		});


	});

})();
(function() {

	'use strict';

	angular.module('ghfs')

	.controller('SupportCtrl', function(  ) {

	});
})();
(function() {

	'use strict';

	angular.module('ghfs')

	.controller('TestCtrl', function( $scope, $http ) {
		var results = $('#results');

		$scope.getPosts = function() {
			$http.get('http://www.greenfundsuriname.org/wp-json/wp/v2/posts/').then(function(response) {
				results.text(JSON.stringify(response.data));
			});
		};

		$scope.getPages = function() {
			$http.get('http://www.greenfundsuriname.org/wp-json/wp/v2/pages/').then(function(response) {
				results.text(JSON.stringify(response.data));
			});
		};

		$scope.getPost = function() {
			$http.get('http://www.greenfundsuriname.org/wp-json/wp/v2/posts/3701').then(function(response) {
				results.text(JSON.stringify(response.data));
			});
		};


	});

})();
(function() {

	'use strict';

	angular.module('ghfs.services', [])

	/**
	 * A simple example service that returns some data.
	 */
	.factory('DataLoader', function( $http ) {

		return {
			get: function(url) {
				// Simple index lookup
				return $http.get( url, {
					headers: {'Accept-Language': 'en-US,en;q=0.8,nl;q=0.6'}
				});
			}
		}
	})


	// Push notification service
	.factory('PushNotificationService', function($http, $rootScope, $q, $timeout, $log) {

		var init = function(options) {

			var q = $q.defer();

			if (!window.cordova && !window.device) {
				$timeout(function() {
					q.reject('Not a native device, will not listen for push notifications');
				}, 100);
				return q.promise;
			}

			var push = PushNotification.init(options);

			// Register new device to receive push notifications
			push.on('registration', function(data) {

				var deviceToken = data.registrationId;
				var os = (ionic.Platform.platform().toLowerCase() == "android") ? "Android" : "iOS";

				return $http.post("http://www.greenfundsuriname.org/pnfw/register", "token=" + deviceToken + "&os=" + os, {
					headers: {
						'Content-Type': 'application/x-www-form-urlencoded'
					}
				}).then(function(deviceToken) {
					$log.debug('Push notification registration success', deviceToken);
					q.resolve(deviceToken);
				}, function(err) {
					$log.error('Push notification registration error', JSON.stringify(err));
					q.reject(err);
				});
			});

			push.on('notification', function(data) {
				$log.log('PushNotificationService: notification event');
				$rootScope.onPushnotificationReceived(data);
			});

			push.on('error', function(e) {
				$log.error('PushNotificationService: push error', e);
				q.reject(e);
			});

			return q.promise;

		};

		return {
			init: init
		};

	});
})();
(function() {

	'use strict';

	angular.module('ghfs.filters')

	.filter('html_filters', function ($sce) {

		return function(text) {

			var htmlObject = document.createElement('div');
			htmlObject.innerHTML = text;

			var links = htmlObject.getElementsByTagName('a');

			for (var i = 0; i < links.length; i++) {

				var link = links[i].getAttribute('href');

				links[i].removeAttribute('href');
				links[i].setAttribute('onclick', 'window.open("'+ link +'", "_system", "location=yes")');
			}

			return $sce.trustAsHtml(htmlObject.outerHTML);
		}

	})


	.filter('trustedhtml', function($sce) {
		return $sce.trustAsHtml;
	})

	.filter('pageTitle', function () {
		return function(str) {
			return str ? str.replace("(APP) ", "") : "";
		};
	});
})();
(function() {

	'use strict';

	angular.module('ghfs.providers')

	.provider('localStorageProvider', function() {

		return {

			$get: ['$window', function($window) {

				return {
					get: function (what) {
						var data = $window.localStorage[what];
						return data ? JSON.parse(data) : false;
					},

					getOne: function(what, key) {
						var data = this.get(what);
						return data.hasOwnProperty(key) ? data[key] : false;
					},

					set: function (what, data) {
						data = JSON.stringify(data);
						$window.localStorage[what] = data;
					},

					erase: function (what) {
						delete $window.localStorage[what];
					}
				}

			}]

		};

	});

})();