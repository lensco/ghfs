(function() {

	'use strict';

	angular.module('ghfs').config(function($stateProvider, $urlRouterProvider, $ionicConfigProvider) {

		$stateProvider

		.state('app', {
			url: "/app",
			abstract: true,
			templateUrl: "templates/menu.html",
			controller: 'AppCtrl'
		})

		.state('app.posts', {
			url: "/posts",
			views: {
				'menuContent': {
					templateUrl: "templates/posts.html",
					controller: 'PostsCtrl'
				}
			}
		})

		.state('app.post', {
			url: "/posts/:postId",
			views: {
				'menuContent': {
					templateUrl: "templates/post.html",
					controller: 'PostCtrl'
				}
			},
			changeColor: 'transparent'
		})

		.state('app.categories', {
			url: "/categories",
			views: {
				'menuContent': {
					templateUrl: "templates/categories.html"
				}
			}
		})

		.state('app.page', {
			url: "/page/:slug",
			views: {
				'menuContent': {
					templateUrl: "templates/page.html",
					controller: "PageCtrl"
				}
			}
		})

		.state('app.programs', {
			url: "/programs",
			views: {
				'menuContent': {
					templateUrl: "templates/programs.html"
				}
			}
		})

		.state('app.settings', {
			url: "/settings",
			views: {
				'menuContent': {
					templateUrl: "templates/settings.html"
				}
			}
		})

		.state('app.test', {
			url: "/test",
			views: {
				'menuContent': {
					templateUrl: "templates/test.html",
					controller: "TestCtrl"
				}
			}
		});

		// if none of the above states are matched, use this as the fallback
		$urlRouterProvider.otherwise('/app/posts');
	});
})();