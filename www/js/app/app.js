/**
 * Ionic GHFS App
 */

angular.module('ghfs.controllers', []);
angular.module('ghfs.services', []);
angular.module('ghfs.filters', []);
angular.module('ghfs.providers', []);

angular.module('ghfs', ['ionic','ionic.service.core', 'ghfs.controllers', 'ghfs.services', 'ghfs.filters', 'ghfs.providers', 'ngCordova'])

.config(function($ionicConfigProvider) {
	// Native scrolling
	if( ionic.Platform.isAndroid() ) {
		$ionicConfigProvider.scrolling.jsScrolling(false);
	}
});


