(function() {

	'use strict';

	angular.module('ghfs').run(function($ionicPlatform, $state, $rootScope, $ionicPopup, $ionicConfig, $log, $http, PushNotificationService, localStorageProvider) {

		$ionicPlatform.ready(function() {
			// Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
			// for form inputs)
			// if (window.cordova && window.cordova.plugins.Keyboard) {
			// 	cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
			// }

			ionic.Platform.fullScreen(true, true);

			if (window.StatusBar) {
				StatusBar.styleLightContent();

				// We let the statusbar overlay the webview, yet the class `in-webview` will move everything down
				// That way we have a transparent statusbar that does not overlap the content
				StatusBar.overlaysWebView(true);

			}

			$rootScope.$on('$stateChangeSuccess', function (evt, toState) {
				$log.log('stateChangeSuccess');

				// check for header color on state change
				if (toState.changeColor) {
					$rootScope.secColor = toState.changeColor;
				} else {
					$rootScope.secColor = false;
				}
			});


			$rootScope.showNetworkError = function() {
				$log.error('Showing network error popup');

				$ionicPopup.alert({
					title: "Network issue",
					template: "It appears your internet connection is down, or there is a problem with the Green Heritage Fund Suriname website. Please try again later…",
					okType: "button-clear button-balanced"
				});
			};


			$ionicConfig.backButton.icon('icon-back');


			// Request push notifications
			if (localStorageProvider.get('deviceToken')) {
				$log.log('PushNotificationService.init: already executed');
			} else {
				PushNotificationService.init({
					"android": {
						"senderID": 952573768849,
						"icon": "notification",
						"iconColor": "#58CF90"
					},
					"ios": {
						"alert": "true",
						"badge": "true",
						"sound": "true",
						"clearBadge": "true"
					}
				}).then(function(deviceToken) {
					$log.log('PushNotificationService.init: resolved: deviceToken=' + deviceToken + '');
					localStorageProvider.set('deviceToken', deviceToken);
				}, function(error) {
					$log.warn('PushNotificationService.init: rejected:', JSON.stringify(error));
				});
			}


			// When push notification is received
			$rootScope.onPushnotificationReceived = function(data) {
				$log.log('PushNotificationService.onPushnotificationReceived', data);

				if(!data.additionalData.foreground) {
					$state.go('app.post', {postId: data.additionalData.id});
				}
			};
		});
	});
})();