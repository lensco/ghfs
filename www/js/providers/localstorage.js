(function() {

	'use strict';

	angular.module('ghfs.providers')

	.provider('localStorageProvider', function() {

		return {

			$get: ['$window', function($window) {

				return {
					get: function (what) {
						var data = $window.localStorage[what];
						return data ? JSON.parse(data) : false;
					},

					getOne: function(what, key) {
						var data = this.get(what);
						return data.hasOwnProperty(key) ? data[key] : false;
					},

					set: function (what, data) {
						data = JSON.stringify(data);
						$window.localStorage[what] = data;
					},

					erase: function (what) {
						delete $window.localStorage[what];
					}
				}

			}]

		};

	});

})();