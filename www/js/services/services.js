(function() {

	'use strict';

	angular.module('ghfs.services', [])

	/**
	 * A simple example service that returns some data.
	 */
	.factory('DataLoader', function( $http ) {

		return {
			get: function(url) {
				// Simple index lookup
				return $http.get( url, {
					headers: {'Accept-Language': 'en-US,en;q=0.8,nl;q=0.6'}
				});
			}
		}
	})


	// Push notification service
	.factory('PushNotificationService', function($http, $rootScope, $q, $timeout, $log) {

		var init = function(options) {

			var q = $q.defer();

			if (!window.cordova && !window.device) {
				$timeout(function() {
					q.reject('Not a native device, will not listen for push notifications');
				}, 100);
				return q.promise;
			}

			var push = PushNotification.init(options);

			// Register new device to receive push notifications
			push.on('registration', function(data) {

				var deviceToken = data.registrationId;
				var os = (ionic.Platform.platform().toLowerCase() == "android") ? "Android" : "iOS";

				return $http.post("http://www.greenfundsuriname.org/pnfw/register", "token=" + deviceToken + "&os=" + os, {
					headers: {
						'Content-Type': 'application/x-www-form-urlencoded'
					}
				}).then(function(deviceToken) {
					$log.debug('Push notification registration success', deviceToken);
					q.resolve(deviceToken);
				}, function(err) {
					$log.error('Push notification registration error', JSON.stringify(err));
					q.reject(err);
				});
			});

			push.on('notification', function(data) {
				$log.log('PushNotificationService: notification event');
				$rootScope.onPushnotificationReceived(data);
			});

			push.on('error', function(e) {
				$log.error('PushNotificationService: push error', e);
				q.reject(e);
			});

			return q.promise;

		};

		return {
			init: init
		};

	});
})();