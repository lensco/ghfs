(function() {

	'use strict';

	angular.module('ghfs')

	.controller('PageCtrl', function($scope, $stateParams, DataLoader, $ionicLoading, $rootScope, $sce, localStorageProvider, $log, $timeout, $ionicConfig, $filter ) {

		$scope.slug = $stateParams.slug;

		var pageApi = $rootScope.url + 'pages/?filter[name]=' + $scope.slug + '&' + $rootScope.callback;

		// Fetch remote page
		$scope.loadPage = function() {
			$ionicLoading.show({
				noBackdrop: true
			});

			$log.log("Fetching '" + pageApi + "'");

			DataLoader.get( pageApi ).then(function(response) {
				var response_data = response.data[0];

				$scope.page = response_data;

				// Don't strip post html
				$scope.content = $sce.trustAsHtml(response_data.content.rendered);

				// Add page to our cache with 1 hour expiration time
				localStorageProvider.set(response_data.slug, { date: new Date().getTime() + (60 * 60 * 1000), data: response_data });

				$ionicLoading.hide();

			}, function(response) {
				$ionicLoading.hide();
				$rootScope.showNetworkError();
			});
		}

		// Check the cache and smell if it's fresh
		var cachedPage = localStorageProvider.get( $scope.slug );

		if( cachedPage && (cachedPage.date > new Date().getTime()) ) {
			// Use cached item
			$log.log("Page '" + $scope.slug + "' is in cache!");
			$scope.page = cachedPage.data;
			$scope.content = $sce.trustAsHtml( $scope.page.content.rendered );

		} else {
			// Go get it again
			$log.log("Page '" + $scope.slug + "' is not in cache, let's go get it!");
			$scope.loadPage();
		}


		$scope.$on('$ionicView.beforeEnter', function() {
			// Just empty back button text until we have a better solution
			$ionicConfig.backButton.text("");
		});


		// Pull to refresh
		$scope.doRefresh = function() {

			$timeout( function() {
				$log.log("Refreshing…");
				$scope.loadPage();

				//Stop the ion-refresher from spinning
				$scope.$broadcast('scroll.refreshComplete');

			}, 1000);

		};

	});
})();