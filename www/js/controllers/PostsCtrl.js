(function() {

	'use strict';

	angular.module('ghfs')

	.controller('PostsCtrl', function( $scope, $http, DataLoader, $timeout, $ionicSlideBoxDelegate, $rootScope, $log, $ionicLoading ) {

		$scope.moreItems = false;
		var paged = 2;

		var postsApi = $rootScope.url + 'posts/?_embed&' + $rootScope.callback;

		// Get all of our posts
		$scope.loadPosts = function() {
			$ionicLoading.show({
				noBackdrop: true
			});

			$log.log("Fetching " + postsApi);

			DataLoader.get( postsApi ).then(function(response) {
				$scope.posts = response.data;

				$scope.doWeHaveUpdates();

				$scope.moreItems = true;

				$ionicLoading.hide();

			}, function(response) {
				$ionicLoading.hide();
				$rootScope.showNetworkError();
			});

		}

		// Let's get some posts
		$scope.loadPosts();


		// Load more (infinite scroll)
		$scope.loadMore = function() {

			if( !$scope.moreItems ) {
				return;
			}

			var pg = paged++;

			$log.log('loadMore ' + pg );

			$timeout(function() {

				DataLoader.get( postsApi + '&page=' + pg ).then(function(response) {

					angular.forEach( response.data, function( value, key ) {
						$scope.posts.push(value);
					});

					if( response.data.length <= 0 ) {
						$scope.moreItems = false;
					}
				}, function(response) {
					$scope.moreItems = false;
					$log.error(response);
				});

				$scope.$broadcast('scroll.infiniteScrollComplete');
				$scope.$broadcast('scroll.resize');

			}, 1000);

		}

		$scope.moreDataExists = function() {
			return $scope.moreItems;
		}

		// Pull to refresh
		$scope.doRefresh = function() {

			$timeout( function() {
				$log.log("Refreshing…");
				$scope.loadPosts();

				//Stop the ion-refresher from spinning
				$scope.$broadcast('scroll.refreshComplete');

			}, 1000);

		};


		// Check if we actually have updates, otherwise show error
		$scope.doWeHaveUpdates = function() {
			if ($scope.posts && $scope.posts.length == 0) {
				$log.error('Posts query returns empty array');

				$scope.weHaveUpdates = false;
			} else {
				$scope.weHaveUpdates = true;
			}
		}

		$scope.$on('$ionicView.enter', function() {
			$scope.doWeHaveUpdates();
		});


	});

})();