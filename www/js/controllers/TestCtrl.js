(function() {

	'use strict';

	angular.module('ghfs')

	.controller('TestCtrl', function( $scope, $http ) {
		var results = $('#results');

		$scope.getPosts = function() {
			$http.get('http://www.greenfundsuriname.org/wp-json/wp/v2/posts/').then(function(response) {
				results.text(JSON.stringify(response.data));
			});
		};

		$scope.getPages = function() {
			$http.get('http://www.greenfundsuriname.org/wp-json/wp/v2/pages/').then(function(response) {
				results.text(JSON.stringify(response.data));
			});
		};

		$scope.getPost = function() {
			$http.get('http://www.greenfundsuriname.org/wp-json/wp/v2/posts/3701').then(function(response) {
				results.text(JSON.stringify(response.data));
			});
		};


	});

})();