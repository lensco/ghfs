(function() {

	'use strict';

	angular.module('ghfs')

	.controller('PostCtrl', function($scope, $stateParams, DataLoader, $ionicLoading, $rootScope, $sce, localStorageProvider, $log, $timeout, $ionicScrollDelegate, $ionicConfig ) {

		$scope.itemID = $stateParams.postId;

		var singlePostApi = $rootScope.url + 'posts/' + $scope.itemID + '?_embed&' + $rootScope.callback;

		// Fetch remote post
		$scope.loadPost = function() {
			$ionicLoading.show({
				noBackdrop: true
			});

			$log.log("Fetching " + singlePostApi);

			DataLoader.get( singlePostApi ).then(function(response) {
				$scope.post = response.data;

				// Don't strip post html
				$scope.content = $sce.trustAsHtml(response.data.content.rendered);

				// Add post to our cache with 1 hour expiration time
				localStorageProvider.set($scope.itemID, { date: new Date().getTime() + (60 * 60 * 1000), data: response.data });

				$ionicLoading.hide();

			}, function(response) {
				$ionicLoading.hide();
				$rootScope.showNetworkError();
			});

		}


		// Check the cache and smell if it's fresh
		var cachedPost = localStorageProvider.get( $scope.itemID );

		if( cachedPost && (cachedPost.date > new Date().getTime()) ) {
			// Use cached item
			$log.log("Post " + $scope.itemID + " is in cache!");
			$scope.post = cachedPost.data;
			$scope.content = $sce.trustAsHtml( $scope.post.content.rendered );

		} else {
			// Go get it again
			$log.log("Post " + $scope.itemID + " is not in cache, let's go get it!");
			$scope.loadPost();
		}


		// Pull to refresh
		$scope.doRefresh = function() {

			$timeout( function() {
				$log.log("Refreshing…");
				$scope.loadPost();

				//Stop the ion-refresher from spinning
				$scope.$broadcast('scroll.refreshComplete');

			}, 1000);

		};


		// Share post
		$scope.sharePost = function(share) {
			$log.log("Sharing post: " + $scope.post.title.rendered, $scope.post.featured_image_thumbnail_url, $scope.post.link)
			if (window.cordova) {
				window.plugins.socialsharing.share(null, $scope.post.title.rendered, $scope.post.featured_image_thumbnail_url, $scope.post.link);
			}
		}


		// Transparent header
		$scope.$on('$ionicView.beforeEnter', function() {
			$('body').addClass('transparent-header');

			// set back button text
			$ionicConfig.backButton.text("Updates");
		});

		$scope.$on('$ionicView.beforeLeave', function() {
			$('body').removeClass('transparent-header');
		});

		$scope.$on('$ionicView.afterEnter', function() {
			var $graphic = $('#featured_image_graphic'),
				graphic_height = $graphic.height(),
				offset = 0,
				alpha = 0,
				$header_backdrop = $('#header-backdrop div');

			$scope.gotScrolled = function() {
				offset = $ionicScrollDelegate.getScrollPosition().top;
				alpha = offset - graphic_height + 164;

				if (alpha > 0 && alpha < 101) {
					$header_backdrop.css('opacity', function() {
						return alpha/100;
					});
				} else if (alpha <= 0) {
					$header_backdrop.css('opacity', 0);
				} else {
					$header_backdrop.css('opacity', 1);
				}
			}
		});

	});
})();