var gulp = require('gulp');
var gutil = require('gulp-util');
var bower = require('bower');
var concat = require('gulp-concat');
var sass = require('gulp-sass');
var stylus = require('gulp-stylus');
var minifyCss = require('gulp-minify-css');
var iconfont = require('gulp-iconfont');
var consolidate = require("gulp-consolidate");
var rename = require('gulp-rename');
var sh = require('shelljs');

var runTimestamp = Math.round(Date.now()/1000);

gulp.task('default', ['styles', 'iconfont', 'js', 'watch']);

// Compile Sass
gulp.task('sass', function() {
	return gulp.src('styles/*.scss')
		.pipe(sass())
		.pipe(gulp.dest('tmp/css'))
});

// Compile Stylus
gulp.task('stylus', function() {
	return gulp.src(['styles/*.styl', '!styles/_*.styl'])
		.pipe(stylus())
		.pipe(gulp.dest('tmp/css'));
});

// Compile all styles
gulp.task('styles', ['sass', 'stylus'], function() {
	return gulp.src(['tmp/css/*'])
		.pipe(concat('app.css'))
		.pipe(gulp.dest('www/css'))
		//.pipe(minifyCss({
		//	keepSpecialComments: 0
		//}))
		//.pipe(rename({ extname: '.min.css' }))
		//.pipe(gulp.dest('www/css'));
});


// Compile javascript
gulp.task('js', function() {
	return gulp.src(['www/js/app/*', 'www/js/controllers/*', 'www/js/services/*', 'www/js/filters/*', 'www/js/providers/*', 'www/js/*', '!www/js/all.js'])
		.pipe(concat('all.js'))
		.pipe(gulp.dest('www/js'))
});


// Generate icon font
gulp.task('iconfont', function() {
	gulp.src(['www/fonts/icons/*.svg'])
		.pipe(iconfont({
			fontName: 'icons',
			appendUnicode: true,
			formats: ['ttf'],
			timestamp: runTimestamp,
			centerHorizontally: true,
			fontHeight: 1024,
			descent: 128,
			ascent: 1024 - 128,
		}))
		.on('glyphs', function(glyphs, options) {
			gulp.src('www/fonts/icons/_icons.styl')
				.pipe(consolidate('lodash', {
					glyphs: glyphs,
					fontName: 'icons',
					fontPath: '../fonts/',
					className: 'icon'
				}))
			.pipe(gulp.dest('styles'));

			gulp.src('www/fonts/icons/_preview.html')
				.pipe(consolidate('lodash', {
					glyphs: glyphs,
					fontName: 'icons',
					fontPath: './',
					className: 'icon'
				}))
			.pipe(rename('preview.html'))
			.pipe(gulp.dest('www/fonts'));
		})
		.pipe(gulp.dest('www/fonts'));
});


// Watch task
gulp.task('watch', function() {
	gulp.watch('styles/*', ['styles']);
	gulp.watch('www/fonts/icons/*.svg', ['iconfont']);
	gulp.watch(['www/js/**/*', '!www/js/all.js'], ['js']);
});



gulp.task('install', ['git-check'], function() {
	return bower.commands.install()
		.on('log', function(data) {
			gutil.log('bower', gutil.colors.cyan(data.id), data.message);
		});
});

gulp.task('git-check', function(done) {
	if (!sh.which('git')) {
		console.log(
			'	' + gutil.colors.red('Git is not installed.'),
			'\n	Git, the version control system, is required to download Ionic.',
			'\n	Download git here:', gutil.colors.cyan('http://git-scm.com/downloads') + '.',
			'\n	Once git is installed, run \'' + gutil.colors.cyan('gulp install') + '\' again.'
		);
		process.exit(1);
	}
	done();
});
